import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommitteeComponent} from './committee/committee.component';
import {HomepageComponent} from './homepage/homepage.component';

const routes: Routes = [{path: '', redirectTo: 'homepage', pathMatch: 'full'}, {
  path: 'committee/:app',
  component: CommitteeComponent
}, {path: 'homepage', component: HomepageComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
