import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {RegisterDialogComponent} from '../register-dialog/register-dialog.component';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: Date;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: new Date(), symbol: 'H'},
  {position: 2, name: 'Helium', weight: new Date(), symbol: 'He'},
  {position: 3, name: 'Lithium', weight: new Date(), symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: new Date(), symbol: 'Be'},
  {position: 5, name: 'Boron', weight: new Date(), symbol: 'B'},
  {position: 6, name: 'Carbon', weight: new Date(), symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: new Date(), symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: new Date(), symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: new Date(), symbol: 'F'},
  {position: 10, name: 'Neon', weight: new Date(), symbol: 'Ne'},
];

@Component({
  selector: 'app-committee',
  templateUrl: './committee.component.html',
  styleUrls: ['./committee.component.scss'],
})
export class CommitteeComponent implements OnInit {

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  columnsToDisplay = ['name', 'weight', 'register'];
  columns = ['name', 'symbol', 'position', 'register'];
  height = '200px';
  imageSources: any[] = ['https://images.unsplash.com/photo-1417436026361-a033044d901f',
    'https://images.unsplash.com/44/MIbCzcvxQdahamZSNQ26_12082014-IMG_3526.jpg'];
  private name = 'test';
  private animal = 'test animal';

  constructor(private router: Router, public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialogBox() {
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });
  }
}
