import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatBadgeModule} from '@angular/material/badge';
import {CommitteeComponent} from './committee/committee.component';
import {HomepageComponent} from './homepage/homepage.component';
import {MatButtonModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatTableModule} from '@angular/material';
import {SlideshowModule} from 'ng-simple-slideshow';
import {RegisterDialogComponent} from './register-dialog/register-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    CommitteeComponent,
    HomepageComponent,
    RegisterDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatBadgeModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    SlideshowModule,
    MatDialogModule
  ],
  providers: [],
  entryComponents: [RegisterDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
